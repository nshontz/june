// Project 3B


// code for password validation
function checkPass() {
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if (pass1.value == pass2.value) {
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    } else {
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}

//code for text area limit

function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
        limitCount.value = limitNum - limitField.value.length;
    }
}

//code for email validation

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {

        return false;
    }

    return true;
}

// Quiz result options in a separate object for flexibility
var resultOptions = [
    {
        title: 'SUPing would be perfect for you!',
        desc: '<img src="http://junen.siteground.net/MTAdventureQuiz/images/SUP.jpg">',
        photo: 'images/sup-meme.png'
    },
    {
        title: 'Hiking',
        desc: '<img src="http://junen.siteground.net/MTAdventureQuiz/images/hike.jpg">',
        photo: 'images/hike-meme.jpg'
    },
    {
        title: 'Paragliding would be perfect for you!',
        desc: '<img src="http://junen.siteground.net/MTAdventureQuiz/images/paraglide1.jpg">',
        photo: 'images/paraglide-meme.png'
    },
    {
        title: 'Whitewater Rafting',
        desc: '><img src="http://junen.siteground.net/MTAdventureQuiz/images/raft.jpg">',
        photo: 'images/raft-meme.png'
    },
    {
        title: 'Mountain Biking would be perfect for you!',
        desc: '<img src="http://junen.siteground.net/MTAdventureQuiz/images/mtb.jpg"/>',
        photo: 'images/mtb-meme.png'
    }
];

$("#email").on("change", function () {
    var email = $(this).val();
    if (!validateEmail(email)) {
        $("#emailHint").removeClass("hide");
        console.log("invalid email");
    } else {
        $("#emailHint").addClass("hide");
        console.log("Valid email");
    }
});
$("#pass1").on("change", function () {
    var password = $(this).val();
    if (password.length < 8) {
        $("#pwd1Hint").removeClass("hide");
        console.log("invalid password");
    } else {
        $("#pwd1Hint").addClass("hide");
        console.log("Valid password");
    }
});

var selectedAdventure = {};
$("#loginForm").on("submit", function (event) {
    event.preventDefault();

    if (typeof(localStorage["user"]) == "undefined") {
        localStorage["user"] = JSON.stringify([]);
    }

    var data = {};
    data.username = $("#username").val();
    data.email = $("#email").val();
    data.password = $("#pass1").val();
    data.quiz = selectedAdventure;

    localStorage["user"] = JSON.stringify(data);
    console.log(data);
    window.location = "meme.html";
});


// global variables
var quizSteps = $('#quizzie .quiz-step'),
    totalScore = 0;

// for each step in the quiz, add the selected answer value to the total score
// if an answer has already been selected, subtract the previous value and update total score with the new selected answer value
// toggle a visual active state to show which option has been selected
quizSteps.each(function () {
    var currentStep = $(this),
        ansOpts = currentStep.children('.quiz-answer');
    // for each option per step, add a click listener
    // apply active class and calculate the total score
    ansOpts.each(function () {
        var eachOpt = $(this);
        eachOpt[0].addEventListener('click', check, false);
        function check() {
            var $this = $(this),
                value = $this.attr('data-quizIndex'),
                answerScore = parseInt(value);
            // check to see if an answer was previously selected
            if (currentStep.children('.active').length > 0) {
                var wasActive = currentStep.children('.active'),
                    oldScoreValue = wasActive.attr('data-quizIndex'),
                    oldScore = parseInt(oldScoreValue);
                // handle visual active state
                currentStep.children('.active').removeClass('active');
                $this.addClass('active');
                // handle the score calculation
                totalScore -= oldScoreValue;
                totalScore += answerScore;
                calcResults(totalScore);
            } else {
                // handle visual active state
                $this.addClass('active');
                // handle score calculation
                totalScore += answerScore;
                calcResults(totalScore);
                // handle current step
                updateStep(currentStep);
            }
        }
    });
});

// show current step/hide other steps
function updateStep(currentStep) {
    if (currentStep.hasClass('current')) {
        currentStep.removeClass('current');
        currentStep.next().addClass('current');
    }
}

// display scoring results
function calcResults(totalScore) {
    // only update the results div if all questions have been answered
    if (quizSteps.find('.active').length == quizSteps.length) {
        var resultsTitle = $('#results h1'),
            resultsDesc = $('#results .desc');

        // calc lowest possible score
        var lowestScoreArray = $('#quizzie .low-value').map(function () {
            return $(this).attr('data-quizIndex');
        });
        var minScore = 0;
        for (var i = 0; i < lowestScoreArray.length; i++) {
            minScore += lowestScoreArray[i] << 0;
        }
        // calculate highest possible score
        var highestScoreArray = $('#quizzie .high-value').map(function () {
            return $(this).attr('data-quizIndex');
        });
        var maxScore = 0;
        for (var i = 0; i < highestScoreArray.length; i++) {
            maxScore += highestScoreArray[i] << 0;
        }
        // calc range, number of possible results, and intervals between results
        var range = maxScore - minScore,
            numResults = resultOptions.length,
            interval = range / (numResults - 1),
            increment = '',
            n = 0; //increment index
        // incrementally increase the possible score, starting at the minScore, until totalScore falls into range. then match that increment index (number of times it took to get totalScore into range) and return the corresponding index results from resultOptions object
        while (n < numResults) {
            increment = minScore + (interval * n);
            if (totalScore <= increment) {
                // populate results
                resultsTitle.replaceWith("<h1>" + resultOptions[n].title + "</h1>");
                resultsDesc.replaceWith("<p class='desc'>" + resultOptions[n].desc + "</p>");
                selectedAdventure = resultOptions[n];
                return;
            } else {
                n++;
            }
        }
    }
}


