/* hw3a.js  */
function transformTranscript() {

    var transcript = document.getElementById("transcriptText").innerHTML;
    var words = transcript.split(" ");
    document.getElementById("transcriptText").innerHTML = "";
    for (var i = 0; i < words.length; i++) {
        if (words[i] != "") {
            var span = document.createElement("span");
            var text = document.createTextNode(words[i] + " ");
            span.appendChild(text);

            span.setAttribute("onmouseover", "this.style.backgroundColor='yellow'");
            span.setAttribute("onmouseout", "this.style.backgroundColor='transparent'");
            document.getElementById("transcriptText").appendChild(span);
        }
    }
}

document.getElementById("divideTranscript").setAttribute("onclick", "transformTranscript()");