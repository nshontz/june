// Project 3B


// code for password validation
function checkPass() {
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if (pass1.value == pass2.value) {
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    } else {
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}

//code for text area limit

function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
        limitCount.value = limitNum - limitField.value.length;
    }
}

//code for phone number validation

var reg = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function PhoneValidation(phoneNumber) {
    var OK = reg.exec(phoneNumber.value);
    if (!OK)
        window.alert("Your phone number isn't valid!");
    else
        window.alert("Phone number is valid");
}

//code for email validation

function validateEmail(emailField) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(emailField.value) == false) {
        alert('Invalid Email Address');
        return false;
    }

    return true;
}

//code for dropdown
function setOptions() {
    var selbox = document.hw4Form.optone;
    var selbox2 = document.hw4Form.opttwo;

    selbox2.options.length = 0;
    chosen = selbox.selectedIndex;
    if (chosen == " ") {
        selbox.options[selbox.options.length] = new Option('Please select one of the options above first', ' ');

    }

    if (chosen == "1") {
        selbox2.options[1] = new
            Option('Instagram', 'oneone');
        selbox2.options[2] = new
            Option('Facebook', 'onetwo');
        selbox2.options[3] = new
            Option('Twitter', 'onethree');
    }
    if (chosen == "2") {
        selbox2.options[1] = new
            Option('Doctor', 'twoone');
        selbox2.options[2] = new
            Option('Lawyer', 'twotwo');
        selbox2.options[3] = new
            Option('Programmer', 'twothree');
    }
    if (chosen == "3") {
        selbox2.options[1] = new
            Option('Oregon', 'threeone');
        selbox2.options[2] = new
            Option('Montana', 'threetwo');
        selbox2.options[3] = new
            Option('Washington', 'threethree');
    }
}

function showWhyInput() {
    document.getElementById("why_best_wrapper").style.display = "block";
}